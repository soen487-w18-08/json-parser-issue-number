PM1.3

(d) Provide a simple JSON reader parser client (GET) for GitHub issues and BitBucket
issues to get a specified issue number, e.g., designing a call getIssueDetails(int
piIssueNumber) and print it to STDOUT (System.out).
• Team 3: https://developer.github.com/v3/issues/
• Team 8:
https://confluence.atlassian.com/bitbucket/issues-resource-296095191.
html